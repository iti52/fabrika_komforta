/**
 * Created on
 */

$(document).ready(function() {
    height_control();
});

$(document).resize(function() {
    height_control();
});

function height_control(){

    if (jQuery(window).width() < 992){
        return false;
    }

    var content_height = jQuery('.main').height();
    var nav_height = jQuery('.navigation').height();

    if (content_height > jQuery(window).height() && content_height > nav_height){
        jQuery('.navigation').css({
            'height': content_height + 'px'
        });
    }
    else if(nav_height < jQuery(window).height()) {
        jQuery('.navigation').css({
            'height': jQuery(window).height() + 'px'
        });
    }
}